/* 
 * File:   main.c
 * Author: husam
 *
 * Created on December 8, 2017, 11:15 PM
 */
#include<16F877A.h>

#device ADC=10

#fuses HS,NOWDT,NOPROTECT,NOBROWNOUT,NOLVP,NOPUT,NOWRT,NOCPD

#use delay (clock=4000000)

#use fast_io(c)
#use fast_io(e)

#define use_portb_lcd TRUE

#include<LCD.C>

#INT_AD

unsigned long int pot0;

float voltaj;

void main(){
    setup_psp(PSP_DISABLED);
    setup_timer_1(T1_DISABLED);
    setup_timer_2(T2_DISABLED,0,1);
    setup_ccp1(CCP_OFF);
    setup_ccp2(CCP_OFF);
    
    set_tris_a(0x0F);
    
    setup_adc(ADC_CLOCK_DIV_32);
    setup_adc_ports(ALL_ANALOG);
    
    lcd_init();
    printf(lcd_putc,"\f DEMETER ");
    delay_ms(1500);
    
    while(1){
        set_adc_channel(2);
        delay_us(20);
        pot0 = read_adc();
        voltaj=5/pot0;
        printf(lcd_putc,"\f AN0 Kanali ");
        delay_ms(1500);
        printf(lcd_putc,"\f Dijital=%1u ",pot0);
        printf(lcd_putc,"\n Voltaj=%fV ",voltaj);
        delay_ms(2500);
        set_adc_channel(3);
        delay_us(20);
        pot0=read_adc();
        voltaj=5/pot0;
        printf(lcd_putc,"\f AN1 Kanali");
        delay_ms(1500);
        printf(lcd_putc,"\f Dijital=%1u",pot0);
        printf(lcd_putc,"\n Voltaj=%fV",voltaj);
        delay_ms(2500);
    }
}
    

