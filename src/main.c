/* 
 * File:   main.c
 * Author: husam
 *
 * Created on December 8, 2017, 11:15 PM
 */
#include<16F877A.h>

#device ADC=10

#fuses HS,NOWDT,NOPROTECT,NOBROWNOUT,NOLVP,NOPUT,NOWRT,NOCPD,NODEBUG

#use delay (clock=4000000)

#define LCD_RS_PIN PIN_B4
#define LCD_RW_PIN PIN_B3
#define LCD_ENABLE_PIN PIN_B2
#define LCD_DATA4 PIN_D7
#define LCD_DATA5 PIN_D6
#define LCD_DATA6 PIN_D5
#define LCD_DATA7 PIN_D4

#include<LCD.C>
unsigned long int sens0;
unsigned long int pot0;

float voltaj;
float sicaklik0;
float set_temp0;
char stat0;

void main(){
    setup_psp(PSP_DISABLED);
    setup_timer_1(T1_DISABLED);
    setup_timer_2(T2_DISABLED,0,1);
    setup_ccp1(CCP_OFF);
    setup_ccp2(CCP_OFF);
    
    set_tris_a(0xFF);
    set_tris_c(0x00);
    
    setup_adc(ADC_CLOCK_DIV_32);
    setup_adc_ports(ALL_ANALOG);
    
    lcd_init();
    printf(lcd_putc,"\f     DEMETER ");
    printf(lcd_putc,"\n   version 0.01");
    delay_ms(1500);
    
    while(1){
        set_adc_channel(4);
        delay_ms(100);
        sens0 = read_adc();
        sicaklik0 = sens0*0.4883;
        
        set_adc_channel(7);
        delay_ms(100);
        pot0 = read_adc();
        set_temp0 = pot0*0.02441+5;
      
        printf(lcd_putc,"\fT0=%4.1f S=%4.1f  ",sicaklik0,set_temp0);
        printf(lcd_putc,"\nRelay %c         ",stat0);
        delay_ms(1000);
        
        if(sicaklik0<(set_temp0-0.5)){
            output_bit(PIN_D1,1);
            stat0='1';
            lcd_gotoxy(16,1);
            lcd_putc(0);
        }
        else if(sicaklik0>(set_temp0+0.5)){
            output_bit(PIN_D1,0);
            stat0='0';        
        }
    }
}
    


