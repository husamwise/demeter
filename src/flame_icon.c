#define e PIN_B0
#define rs PIN_B2

void lcd_komut(byte komut){
    unsigned int reverse_komut = 0, i, temp;
 
    for (i = 0; i < 8; i++)
    {
        temp = (komut & (1 << i));
        if(temp)
            reverse_komut |= (1 << (7 - i));
    }
    output_d(reverse_komut)
    output_low(rs);
    delay_cycles(1);
    output_high(e);
    delay_cycles(1);
    output_low(e);
    delay_ms(2);
    
    output_d(reverse_komut>>4);
    output_low(rs);
    delay_cycles(1);
    output_high(e);
    delay_cycles(1);
    output_low(e);
    delay_ms(2);
}

void lcd_veri(byte veri){
    unsigned int reverse_veri = 0, i, temp;
 
    for (i = 0; i < 8; i++)
    {
        temp = (veri & (1 << i));
        if(temp)
            reverse_veri |= (1 << (7 - i));
    }
    
    output_d(reverse_veri)
    output_high(rs);
    delay_cycles(1);
    output_high(e);
    delay_cycles(1);
    output_low(e);
    delay_ms(2);
    
    output_d(reverse_veri>>4);
    output_high(rs);
    delay_cycles(1);
    output_high(e);
    delay_cycles(1);
    output_low(e);
    delay_ms(2);
    
}

void flame_write(){
    lcd_komut(0x40);lcd_veri(2);
    lcd_komut(0x41);lcd_veri(6);
    lcd_komut(0x42);lcd_veri(7);
    lcd_komut(0x43);lcd_veri(11);
    lcd_komut(0x44);lcd_veri(21);
    lcd_komut(0x45);lcd_veri(30);
    lcd_komut(0x46);lcd_veri(11);
    lcd_komut(0x47);lcd_veri(6);
}
